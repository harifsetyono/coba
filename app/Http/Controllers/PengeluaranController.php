<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use App\Kategori;
use App\Kas;
use Alert;

class PengeluaranController extends Controller
{

  public function index()
  {
    $pengeluaran = Kas::join('kategori','kategori.kategori_id','=','kas.kategori_id')
                ->where('kas_jenis','=','2')
                ->orderBy('kas_id','DESC')
                ->get();
    return view('admin.pengeluaran.index', compact('pengeluaran'));
  }

  public function create()
  {
    $kategori = Kategori::where('kategori_jenis','=','2')->get();
    return view('admin.pengeluaran.create', compact('kategori'));
  }

  public function store(Request $request)
  {

    $max_id = DB::table('kas')->max('kas_id');

    if (empty($max_id)) {
      $max_id = 0;
    }else {
      $max_id = $max_id;
    }

    $saldo_terakhir = Kas::where('kas_id','=',$max_id)->value('kas_saldo');
    if (empty($saldo_terakhir)) {
      $saldo_terakhir = 0;
    }else {
      $saldo_terakhir = $saldo_terakhir;
    }



    $kas = new Kas;
    $kategori_id = $request['kategori_id'];
    $kas_tanggal = $request['kas_tanggal'];
    $kas_keterangan = $request['kas_keterangan'];
    $kas_masuk = "0";
    $kas_keluar = $request['kas_keluar'];
    $kas_saldo = $saldo_terakhir-$kas_keluar;
    $kas_jenis = "2";


    $kas->kategori_id = $kategori_id;
    $kas->kas_tanggal = $kas_tanggal;
    $kas->kas_keterangan = $kas_keterangan;
    $kas->kas_masuk = $kas_masuk;
    $kas->kas_keluar = $kas_keluar;
    $kas->kas_saldo = $kas_saldo;
    $kas->kas_jenis = $kas_jenis;
    $kas->save();


    Alert::success('Data berhasil ditambah', 'Success');
    return redirect('pengeluaran');

    // dd($kas_saldo);
  }

  public function edit($id)
  {
    $pengeluaran = Kas::where('kas_id','=',$id)->get();
    $kategori = Kategori::where('kategori_jenis','=','2')->get();
    // dd($pemasukan);
    return view('admin.pengeluaran.edit', compact('kategori','pengeluaran'));
  }

  public function update(Request $request)
  {

    $kas_id = $request['kas_id'];
    $kategori_id = $request['kategori_id'];
    $kas_tanggal = $request['kas_tanggal'];
    $kas_keterangan = $request['kas_keterangan'];
    $kas_keluar = $request['kas_keluar'];

    $kas = Kas::find($kas_id);

    $kas->kategori_id = $kategori_id;
    $kas->kas_tanggal = $kas_tanggal;
    $kas->kas_keterangan = $kas_keterangan;
    $kas->kas_keluar = $kas_keluar;
    $kas->update();


    return redirect('pengeluaran');
  }

  public function delete($id)
  {

    $kas = Kas::find($id);
    $kas->delete();

    return redirect('pengeluaran');
  }

}
