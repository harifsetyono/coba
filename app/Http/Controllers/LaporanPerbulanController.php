<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use Redirect;
use App\Kategori;
use App\Kas;

class LaporanPerbulanController extends Controller
{
  public function index()
  {

    $tot_masuk = DB::table('kas')->sum('kas_masuk');
    $tot_keluar = DB::table('kas')->sum('kas_keluar');
    $saldo_terakhir = $tot_masuk-$tot_keluar;

    return view('admin.laporan_perbulan.index', compact('saldo_terakhir'));

  }

  public function cari(Request $request)
  {
    $bulan = $request['bulan'];
    $tahun = $request['tahun'];

    $bulan_tampil = $bulan;
    $tahun_tampil = $tahun;


    //saldo terakhir
    $tot_masuk = DB::table('kas')->sum('kas_masuk');
    $tot_keluar = DB::table('kas')->sum('kas_keluar');
    $saldo_terakhir = $tot_masuk-$tot_keluar;


    //total pemasukan bulan ini
    $total_pemasukan = DB::table('kas')
    ->whereMonth('kas_tanggal', $bulan)
    ->whereYear('kas_tanggal', $tahun)
    ->where('kas_jenis','=','1')
    ->sum('kas_masuk');


    //total pengeluaran bulan ini
    $total_pengeluaran = DB::table('kas')
    ->whereMonth('kas_tanggal', $bulan)
    ->whereYear('kas_tanggal', $tahun)
    ->where('kas_jenis','=','2')
    ->sum('kas_keluar');

    //saldo bulan ini
    $saldo_bulan_ini = $total_pemasukan-$total_pengeluaran;

    //laporan
    $laporan = DB::select("SELECT kas.kas_id, kas.kas_tanggal, kas.kas_keterangan, kat.kategori_nama, kas.kas_masuk, kas.kas_keluar, @saldo:=@saldo+kas_masuk-kas_keluar AS kas_saldo
                  FROM kas
                  JOIN kategori kat ON kas.kategori_id = kat.kategori_id
                  JOIN (
                  	SELECT @saldo:=0
                  ) t3
                  WHERE
                  MONTH(kas.kas_tanggal) = '$bulan' AND YEAR(kas.kas_tanggal) = '$tahun';");

    return view('admin.laporan_perbulan.cari', compact('laporan','saldo_terakhir','bulan_tampil','tahun_tampil', 'total_pemasukan', 'total_pengeluaran', 'saldo_bulan_ini'));

    // dd($saldo_bulan_ini);
  }

  public function laporanPerBulanPdf($bulan,$tahun)
  {


    $bulan_tampil = $bulan;
    $tahun_tampil = $tahun;


    //saldo terakhir
    $tot_masuk = DB::table('kas')->sum('kas_masuk');
    $tot_keluar = DB::table('kas')->sum('kas_keluar');
    $saldo_terakhir = $tot_masuk-$tot_keluar;


    //total pemasukan bulan ini
    $total_pemasukan = DB::table('kas')
    ->whereMonth('kas_tanggal', $bulan)
    ->whereYear('kas_tanggal', $tahun)
    ->where('kas_jenis','=','1')
    ->sum('kas_masuk');


    //total pengeluaran bulan ini
    $total_pengeluaran = DB::table('kas')
    ->whereMonth('kas_tanggal', $bulan)
    ->whereYear('kas_tanggal', $tahun)
    ->where('kas_jenis','=','2')
    ->sum('kas_keluar');

    //saldo bulan ini
    $saldo_bulan_ini = $total_pemasukan-$total_pengeluaran;

    //laporan
    $laporan = DB::select("SELECT kas.kas_id, kas.kas_tanggal, kas.kas_keterangan, kat.kategori_nama, kas.kas_masuk, kas.kas_keluar, @saldo:=@saldo+kas_masuk-kas_keluar AS kas_saldo
                  FROM kas
                  JOIN kategori kat ON kas.kategori_id = kat.kategori_id
                  JOIN (
                  	SELECT @saldo:=0
                  ) t3
                  WHERE
                  MONTH(kas.kas_tanggal) = '$bulan' AND YEAR(kas.kas_tanggal) = '$tahun';");

    $pdf = PDF::loadView('admin.laporan_perbulan.pdf',compact('laporan','saldo_terakhir','bulan_tampil','tahun_tampil', 'total_pemasukan', 'total_pengeluaran', 'saldo_bulan_ini'));
    $pdf->setPaper('a4', 'potrait');
    return $pdf->stream();
  }
}
