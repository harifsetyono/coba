<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use Redirect;
use App\Kategori;
use App\Kas;

class LaporanPerbulanController extends Controller
{
  public function index()
  {

    //lama==========================================================

    // $max_id = DB::table('kas')->max('kas_id');
    //
    // if (empty($max_id)) {
    //   $max_id = 0;
    // }else {
    //   $max_id = $max_id;
    // }
    //
    // $saldo_terakhir = Kas::where('kas_id','=',$max_id)->value('kas_saldo');
    // if (empty($saldo_terakhir)) {
    //   $saldo_terakhir = 0;
    // }else {
    //   $saldo_terakhir = $saldo_terakhir;
    // }
    //
    // return view('admin.laporan_perbulan.index', compact('saldo_terakhir'));

    //Baru =====================================================================

    $tot_masuk = DB::table('kas')->sum('kas_masuk');
    $tot_keluar = DB::table('kas')->sum('kas_keluar');
    $saldo_terakhir = $tot_masuk-$tot_keluar;

    return view('admin.laporan_perbulan.index', compact('saldo_terakhir'));

    // dd($saldo_terakhir);



  }

  public function cari(Request $request)
  {
    $bulan = $request['bulan'];
    $tahun = $request['tahun'];

    $bulan_tampil = $bulan;
    $tahun_tampil = $tahun;

    $bulan_sebelumnya = 0;
    $tahun_sebelumnya = 0;
    if ($bulan == 1) {
      $bulan_sebelumnya = 12;
      $tahun_sebelumnya = $tahun-1;

      $max_id = DB::table('kas')
      ->whereMonth('kas_tanggal', $bulan_sebelumnya)
      ->whereYear('kas_tanggal', $tahun_sebelumnya)
      ->max('kas_id');

      if (empty($max_id)) {
        $max_id = 0;
      }else {
        $max_id = $max_id;
      }

      $saldo_bulan_sebelumnya = Kas::where('kas_id','=',$max_id)->value('kas_saldo');
      if (empty($saldo_bulan_sebelumnya)) {
        $saldo_bulan_sebelumnya = 0;
      }else {
        $saldo_bulan_sebelumnya = $saldo_bulan_sebelumnya;
      }
    }else {
      $bulan_sebelumnya = $bulan-1;
      $tahun_sebelumnya = $tahun;

      $max_id = DB::table('kas')
      ->whereMonth('kas_tanggal', $bulan_sebelumnya)
      ->whereYear('kas_tanggal', $tahun_sebelumnya)
      ->max('kas_id');

      if (empty($max_id)) {
        $max_id = 0;
      }else {
        $max_id = $max_id;
      }

      $saldo_bulan_sebelumnya = Kas::where('kas_id','=',$max_id)->value('kas_saldo');
      if (empty($saldo_bulan_sebelumnya)) {
        $saldo_bulan_sebelumnya = 0;
      }else {
        $saldo_bulan_sebelumnya = $saldo_bulan_sebelumnya;
      }
    }



    //saldo terakhir
    $id1 = DB::table('kas')->max('kas_id');

    if (empty($id1)) {
      $id1 = 0;
    }else {
      $id1 = $id1;
    }

    $saldo_terakhir = Kas::where('kas_id','=',$id1)->value('kas_saldo');
    if (empty($saldo_terakhir)) {
      $saldo_terakhir = 0;
    }else {
      $saldo_terakhir = $saldo_terakhir;
    }

    //saldo bulan ini
    $id = DB::table('kas')
    ->whereMonth('kas_tanggal', $bulan)
    ->whereYear('kas_tanggal', $tahun)
    ->max('kas_id');

    if (empty($id)) {
      $id = 0;
    }else {
      $id = $id;
    }

    $saldo_bulan_ini = Kas::where('kas_id','=',$id)->value('kas_saldo');
    if (empty($saldo_bulan_ini)) {
      $saldo_bulan_ini = 0;
    }else {
      $saldo_bulan_ini = $saldo_bulan_ini;
    }

    //total pemasukan bulan ini
    $total_pemasukan = DB::table('kas')
    ->whereMonth('kas_tanggal', $bulan)
    ->whereYear('kas_tanggal', $tahun)
    ->where('kas_jenis','=','1')
    ->sum('kas_masuk');


    //total pengeluaran bulan ini
    $total_pengeluaran = DB::table('kas')
    ->whereMonth('kas_tanggal', $bulan)
    ->whereYear('kas_tanggal', $tahun)
    ->where('kas_jenis','=','2')
    ->sum('kas_keluar');

    //laporan
    $laporan = Kas::join('kategori', 'kategori.kategori_id','=','kas.kategori_id')
    ->whereMonth('kas_tanggal', $bulan)
    ->whereYear('kas_tanggal', $tahun)
    ->orderBy('kas_id','ASC')
    ->get();

    return view('admin.laporan_perbulan.cari', compact('laporan','saldo_terakhir','saldo_bulan_sebelumnya','bulan_tampil','tahun_tampil','bulan_sebelumnya', 'tahun_sebelumnya', 'total_pemasukan', 'total_pengeluaran', 'saldo_bulan_ini'));

  }

  public function laporanPerBulanPdf($bulan,$tahun)
  {

    $bulan_tampil = $bulan;
    $tahun_tampil = $tahun;

    $bulan_sebelumnya = 0;
    $tahun_sebelumnya = 0;
    if ($bulan == 1) {
      $bulan_sebelumnya = 12;
      $tahun_sebelumnya = $tahun-1;

      $max_id = DB::table('kas')
      ->whereMonth('kas_tanggal', $bulan_sebelumnya)
      ->whereYear('kas_tanggal', $tahun_sebelumnya)
      ->max('kas_id');

      if (empty($max_id)) {
        $max_id = 0;
      }else {
        $max_id = $max_id;
      }

      $saldo_bulan_sebelumnya = Kas::where('kas_id','=',$max_id)->value('kas_saldo');
      if (empty($saldo_bulan_sebelumnya)) {
        $saldo_bulan_sebelumnya = 0;
      }else {
        $saldo_bulan_sebelumnya = $saldo_bulan_sebelumnya;
      }
    }else {
      $bulan_sebelumnya = $bulan-1;
      $tahun_sebelumnya = $tahun;

      $max_id = DB::table('kas')
      ->whereMonth('kas_tanggal', $bulan_sebelumnya)
      ->whereYear('kas_tanggal', $tahun_sebelumnya)
      ->max('kas_id');

      if (empty($max_id)) {
        $max_id = 0;
      }else {
        $max_id = $max_id;
      }

      $saldo_bulan_sebelumnya = Kas::where('kas_id','=',$max_id)->value('kas_saldo');
      if (empty($saldo_bulan_sebelumnya)) {
        $saldo_bulan_sebelumnya = 0;
      }else {
        $saldo_bulan_sebelumnya = $saldo_bulan_sebelumnya;
      }
    }



    //saldo terakhir
    $id1 = DB::table('kas')->max('kas_id');

    if (empty($id1)) {
      $id1 = 0;
    }else {
      $id1 = $id1;
    }

    $saldo_terakhir = Kas::where('kas_id','=',$id1)->value('kas_saldo');
    if (empty($saldo_terakhir)) {
      $saldo_terakhir = 0;
    }else {
      $saldo_terakhir = $saldo_terakhir;
    }

    //saldo bulan ini
    $id = DB::table('kas')
    ->whereMonth('kas_tanggal', $bulan)
    ->whereYear('kas_tanggal', $tahun)
    ->max('kas_id');

    if (empty($id)) {
      $id = 0;
    }else {
      $id = $id;
    }

    $saldo_bulan_ini = Kas::where('kas_id','=',$id)->value('kas_saldo');
    if (empty($saldo_bulan_ini)) {
      $saldo_bulan_ini = 0;
    }else {
      $saldo_bulan_ini = $saldo_bulan_ini;
    }

    //total pemasukan bulan ini
    $total_pemasukan = DB::table('kas')
    ->whereMonth('kas_tanggal', $bulan)
    ->whereYear('kas_tanggal', $tahun)
    ->where('kas_jenis','=','1')
    ->sum('kas_masuk');


    //total pengeluaran bulan ini
    $total_pengeluaran = DB::table('kas')
    ->whereMonth('kas_tanggal', $bulan)
    ->whereYear('kas_tanggal', $tahun)
    ->where('kas_jenis','=','2')
    ->sum('kas_keluar');

    //laporan
    $laporan = Kas::join('kategori', 'kategori.kategori_id','=','kas.kategori_id')
    ->whereMonth('kas_tanggal', $bulan)
    ->whereYear('kas_tanggal', $tahun)
    ->get();

    $pdf = PDF::loadView('admin.laporan_perbulan.pdf',compact('laporan','saldo_terakhir','saldo_bulan_sebelumnya','bulan_tampil','tahun_tampil','bulan_sebelumnya', 'tahun_sebelumnya', 'total_pemasukan', 'total_pengeluaran', 'saldo_bulan_ini'));
    $pdf->setPaper('a4', 'potrait');
    return $pdf->stream();
  }
}
