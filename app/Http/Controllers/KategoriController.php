<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use App\Kategori;
use Alert;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      return view('admin.kategori.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $kategori = new Kategori;
      $kategori ->kategori_nama = $request['kategori_nama'];
      $kategori ->kategori_jenis = $request['kategori_jenis'];
      $kategori -> save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $kategori = Kategori::find($id);
      echo json_encode($kategori);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $kategori = Kategori::find($id);
      $kategori ->kategori_nama = $request['kategori_nama'];
      $kategori ->kategori_jenis = $request['kategori_jenis'];
      $kategori -> update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $kategori = Kategori::find($id);
      $kategori -> delete();
    }

    public function listData()
    {
        $kategori = Kategori::orderBy('kategori_id', 'DESC')->get();
        $no = 0;
        $data = array();
        $kategori_jenis = "";
        foreach ($kategori as $list) {

            if ($list->kategori_jenis==1) {
              $kategori_jenis = "Masuk";
            }else if ($list->kategori_jenis==2) {
              $kategori_jenis = "Keluar";
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->kategori_nama;
            $row[] = $kategori_jenis;
            $row[] = '<a onclick="editForm('.$list->kategori_id.')" class="btn btn-primary" data-toggle="tooltip" data-placement="botttom" title="Edit Data"  style="color:white;"><i class="fa  fa-edit"></i></a>
            <a onclick="deleteData('.$list->kategori_id.')" class="btn btn-danger" data-toggle="tooltip" data-placement="botttom" title="Hapus Data" style="color:white;"><i class="fa  fa-trash"></i></a>';
            $data[] = $row;

        }

        $output = array("data" => $data);
        return response()->json($output);

    }

}
