<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use App\Kategori;
use App\Kas;
use Alert;

class PemasukanController extends Controller
{

  public function index()
  {
    $pemasukan = Kas::join('kategori','kategori.kategori_id','=','kas.kategori_id')
                ->where('kas_jenis','=','1')
                ->orderBy('kas_id','DESC')
                ->get();
                // dd($pemasukan);
    return view('admin.pemasukan.index', compact('pemasukan'));
  }

  public function create()
  {
    $kategori = Kategori::where('kategori_jenis','=','1')->get();
    return view('admin.pemasukan.create', compact('kategori'));
  }

  public function store(Request $request)
  {

      $max_id = DB::table('kas')->max('kas_id');

      if (empty($max_id)) {
        $max_id = 0;
      }else {
        $max_id = $max_id;
      }

      $saldo_terakhir = Kas::where('kas_id','=',$max_id)->value('kas_saldo');
      if (empty($saldo_terakhir)) {
        $saldo_terakhir = 0;
      }else {
        $saldo_terakhir = $saldo_terakhir;
      }

      // dd($saldo_terakhir);


      $kas = new Kas;
      $kategori_id = $request['kategori_id'];
      $kas_tanggal = $request['kas_tanggal'];
      $kas_keterangan = $request['kas_keterangan'];
      $kas_masuk = $request['kas_masuk'];
      $kas_keluar = "0";
      $kas_saldo = $saldo_terakhir+$kas_masuk;
      $kas_jenis = "1";


      $kas->kategori_id = $kategori_id;
      $kas->kas_tanggal = $kas_tanggal;
      $kas->kas_keterangan = $kas_keterangan;
      $kas->kas_masuk = $kas_masuk;
      $kas->kas_keluar = $kas_keluar;
      $kas->kas_saldo = $kas_saldo;
      $kas->kas_jenis = $kas_jenis;
      $kas->save();


      Alert::success('Data berhasil ditambah', 'Success');
      return redirect('pemasukan');
  }

  public function edit($id)
  {
    $pemasukan = Kas::where('kas_id','=',$id)->get();
    $kategori = Kategori::where('kategori_jenis','=','1')->get();
    // dd($pemasukan);
    return view('admin.pemasukan.edit', compact('kategori','pemasukan'));
  }

  public function update(Request $request)
  {

    $kas_id = $request['kas_id'];
    $kategori_id = $request['kategori_id'];
    $kas_tanggal = $request['kas_tanggal'];
    $kas_keterangan = $request['kas_keterangan'];
    $kas_masuk = $request['kas_masuk'];

    $kas = Kas::find($kas_id);

    $kas->kategori_id = $kategori_id;
    $kas->kas_tanggal = $kas_tanggal;
    $kas->kas_keterangan = $kas_keterangan;
    $kas->kas_masuk = $kas_masuk;
    $kas->update();


    return redirect('pemasukan');
  }

  public function delete($id)
  {

    $kas = Kas::find($id);
    $kas->delete();

    return redirect('pemasukan');
  }



}
