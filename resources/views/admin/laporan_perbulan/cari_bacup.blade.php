<?php $hal = "laporan_perbulan"; ?>
@extends('layouts.admin.master')
@section('title', 'Tambah pemasukan')

@section('css')
<link rel="stylesheet" href="{{asset('public/admin/bower_components/select2/dist/css/select2.min.css')}}">

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan Kas
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- Horizontal Form -->
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title pull-right"> <span style="background:green; padding:5px; color:white;">Saldo akhir : Rp. {{number_format($saldo_terakhir,0,',','.')}}</span> </h3>
        </div>
        <!-- /.box-header -->

        <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          Note : <strong class="d-block d-sm-inline-block-force">Lakukan pencarian berdasarkan Bulan dan Tahun !</strong>
        </div><!-- alert -->

        <form class="" action="{{route('laporan_perbulan_cari.cari')}}" method="post">
          {{csrf_field()}}



          <div class="box-body">
            <div class="row">
              <div class="col-xs-4">

                <div class="row">
                  <div class="col-xs-2">
                    <label for="ex1">Bulan</label>
                  </div>
                  <div class="col-xs-10">
                    <select name="bulan" class="form-control">
                      <?php
                      $bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                      for($a=1;$a<=12;$a++){
                        if($a==$bulan_tampil)
                        {
                          $pilih="selected";
                        }
                        else
                        {
                          $pilih="";
                        }
                        echo("<option value=\"$a\" $pilih>$bulan[$a]</option>"."\n");
                      }
                      ?>
                    </select>
                  </div>
                </div>



              </div>
              <div class="col-xs-4">
                <div class="row">
                  <div class="col-xs-2">
                    <label for="ex1">Tahun</label>
                  </div>
                  <div class="col-xs-10">
                    <select id="tahun" name="tahun" class="form-control">
                      <?php
                      $mulai= date('Y') - 5;
                      for($i = $mulai;$i<$mulai + 11;$i++){
                        $sel = $i == $tahun_tampil ? ' selected="selected"' : '';
                        echo '<option value="'.$i.'"'.$sel.'>'.$i.'</option>';
                      }
                      ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-xs-4">
                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Cari </button>
                <a target="_blank" href="{{url('laporan_perbulan_pdf')}}/{{$bulan_tampil}}/{{$tahun_tampil}}" class="btn btn-warning"><i class="fa fa-print"></i> Cetak</a>
              </div>
            </div>
          </div>


        </form>



        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th style="width:5%;">No #</th>
                <th style="width:10%;">Tanggal</th>
                <th style="width:25%;">Keterangan</th>
                <th style="width:15%;">Kat. Jenis</th>
                <th style="width:15%;">Masuk (Rp.)</th>
                <th style="width:15%;">Keluar (Rp.)</th>
                <th style="width:15%;">Saldo (Rp.)</th>
              </tr>
            </thead>
            <tbody>
              <tr>

                <?php


                $bulan_text = "";

                if ($bulan_sebelumnya == "1") {
                  $bulan_text = "Januari";
                }elseif ($bulan_sebelumnya == "2") {
                  $bulan_text = "Febuari";
                }elseif ($bulan_sebelumnya == "3") {
                  $bulan_text = "Maret";
                }elseif ($bulan_sebelumnya == "4") {
                  $bulan_text = "April";
                }elseif ($bulan_sebelumnya == "5") {
                  $bulan_text = "Mei";
                }elseif ($bulan_sebelumnya == "6") {
                  $bulan_text = "Juni";
                }elseif ($bulan_sebelumnya == "7") {
                  $bulan_text = "Juli";
                }elseif ($bulan_sebelumnya == "8") {
                  $bulan_text = "Agustus";
                }elseif ($bulan_sebelumnya == "9") {
                  $bulan_text = "September";
                }elseif ($bulan_sebelumnya == "10") {
                  $bulan_text = "Oktober";
                }elseif ($bulan_sebelumnya == "11") {
                  $bulan_text = "November";
                }elseif ($bulan_sebelumnya == "12") {
                  $bulan_text = "Desember";
                }

                ?>

                <td> 1</td>
                <td>01/{{$bulan_tampil}}/{{$tahun_tampil}}</td>
                <td>Kas bulan {{$bulan_text}} {{$tahun_sebelumnya}}</td>
                <td>Sisa Kas</td>
                <td style="text-align:right;">-</td>
                <td style="text-align:right;">-</td>
                <td style="text-align:right;">{{number_format($saldo_bulan_sebelumnya,0,',','.')}}</td>
              </tr>
              <?php $no = 2; ?>
              @foreach($laporan as $data)

              <?php

              $masuk = "";
              $keluar = "";

              if ($data->kas_masuk==0) {
                $masuk = "-";
              }else {
                $masuk = number_format($data->kas_masuk,0,',','.');
              }

              if ($data->kas_keluar==0) {
                $keluar = "-";
              }else {
                $keluar = number_format($data->kas_keluar,0,',','.');
              }

              ?>

              <tr>
                <td>{{$no}}</td>
                <td>{{date('d/m/Y', strtotime($data->kas_tanggal))}}</td>
                <td>{{$data->kas_keterangan}}</td>
                <td>{{$data->kategori_nama}}</td>
                <td style="text-align:right;">{{$masuk}}</td>
                <td style="text-align:right;">{{$keluar}}</td>
                <td style="text-align:right;">{{number_format($data->kas_saldo,0,',','.')}}</td>
              </tr>

              <?php $no++; ?>
              @endforeach


            </tbody>
            <tfoot>
              <th style="text-align:center;" colspan="4">TOTAL</th>
              <th style="text-align:right;">{{number_format($total_pemasukan,0,',','.')}} </th>
              <th style="text-align:right;">{{number_format($total_pengeluaran,0,',','.')}} </th>
              <th style="text-align:right;">{{number_format($saldo_bulan_ini,0,',','.')}} </th>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->

      </div>
      <!-- /.box -->


    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@endsection


@section('js')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script> -->
<script src="{{asset('public/admin/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{ asset('public/js/sweetalert.min.js') }}"></script>
@include('sweet::alert')

<script>
// $(function () {
//   //Initialize Select2 Elements
//   $('.select2').select2()
// })
$(document).ready(function() {
  $('.js-example-basic-single').select2({
    // dropdownParent: $(".modal")
  });
});
</script>

<script>
function hanyaAngka(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))

  return false;
  return true;
}
</script>

<script type="text/javascript">
var kas_masuk = document.getElementById("kas_masuk");
// var rupiah = document.getElementById("rupiah");
var rupiah = document.getElementById("rupiah");
kas_masuk.addEventListener("keyup", function(e) {
  // tambahkan 'Rp.' pada saat form di ketik
  // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
  // rupiah.value = formatRupiah(this.value, "Rp. ");
  rupiah.innerHTML = formatRupiah(this.value, "Rp. ");
  $('#rupiah').val('coba');
  console.log(rupiah);
});

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix) {
  var number_string = angka.replace(/[^,\d]/g, "").toString(),
  split = number_string.split(","),
  sisa = split[0].length % 3,
  kas_masuk = split[0].substr(0, sisa),
  ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if (ribuan) {
    separator = sisa ? "." : "";
    kas_masuk += separator + ribuan.join(".");
  }

  kas_masuk = split[1] != undefined ? kas_masuk + "," + split[1] : kas_masuk;
  return prefix == undefined ? kas_masuk : kas_masuk ? "Rp. " + kas_masuk : "";
}
</script>

@endsection
