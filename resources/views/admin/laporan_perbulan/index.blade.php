<?php $hal = "laporan_perbulan"; ?>
@extends('layouts.admin.master')
@section('title', 'Laporan Kas')

@section('css')
<link rel="stylesheet" href="{{asset('public/admin/bower_components/select2/dist/css/select2.min.css')}}">

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan Kas
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- Horizontal Form -->
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title pull-right"><span style="background:green; padding:5px; color:white;">Saldo akhir : Rp. {{number_format($saldo_terakhir,0,',','.')}}</span></h3>
        </div>
        <!-- /.box-header -->

        <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          Note : <strong class="d-block d-sm-inline-block-force">Lakukan pencarian berdasarkan Bulan dan Tahun !</strong>
        </div><!-- alert -->

        <form class="" action="{{route('laporan_perbulan_cari.cari')}}" method="post">
          {{csrf_field()}}



          <div class="box-body">
            <div class="row">
              <div class="col-xs-4">

                <div class="row">
                  <div class="col-xs-2">
                    <label for="ex1">Bulan</label>
                  </div>
                  <div class="col-xs-10">
                    <select name="bulan" class="form-control">
                      <?php
                      $bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                      for($a=1;$a<=12;$a++){
                        if($a==date("m"))
                        {
                          $pilih="selected";
                        }
                        else
                        {
                          $pilih="";
                        }
                        echo("<option value=\"$a\" $pilih>$bulan[$a]</option>"."\n");
                      }
                      ?>
                    </select>
                  </div>
                </div>



              </div>
              <div class="col-xs-4">
                <div class="row">
                  <div class="col-xs-2">
                    <label for="ex1">Tahun</label>
                  </div>
                  <div class="col-xs-10">
                    <select id="tahun" name="tahun" class="form-control">
                      <?php
                      $mulai= date('Y') - 5;
                      for($i = $mulai;$i<$mulai + 11;$i++){
                        $sel = $i == date('Y') ? ' selected="selected"' : '';
                        echo '<option value="'.$i.'"'.$sel.'>'.$i.'</option>';
                      }
                      ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-xs-4">
                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Cari </button>
              </div>
            </div>
          </div>


        </form>

        <br><br>

      </div>
      <!-- /.box -->


    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@endsection


@section('js')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script> -->
<script src="{{asset('public/admin/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{ asset('public/js/sweetalert.min.js') }}"></script>
@include('sweet::alert')

<script>
// $(function () {
//   //Initialize Select2 Elements
//   $('.select2').select2()
// })
$(document).ready(function() {
  $('.js-example-basic-single').select2({
    // dropdownParent: $(".modal")
  });
});
</script>

<script>
function hanyaAngka(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))

  return false;
  return true;
}
</script>

<script type="text/javascript">
var kas_masuk = document.getElementById("kas_masuk");
// var rupiah = document.getElementById("rupiah");
var rupiah = document.getElementById("rupiah");
kas_masuk.addEventListener("keyup", function(e) {
  // tambahkan 'Rp.' pada saat form di ketik
  // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
  // rupiah.value = formatRupiah(this.value, "Rp. ");
  rupiah.innerHTML = formatRupiah(this.value, "Rp. ");
  $('#rupiah').val('coba');
  console.log(rupiah);
});

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix) {
  var number_string = angka.replace(/[^,\d]/g, "").toString(),
  split = number_string.split(","),
  sisa = split[0].length % 3,
  kas_masuk = split[0].substr(0, sisa),
  ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if (ribuan) {
    separator = sisa ? "." : "";
    kas_masuk += separator + ribuan.join(".");
  }

  kas_masuk = split[1] != undefined ? kas_masuk + "," + split[1] : kas_masuk;
  return prefix == undefined ? kas_masuk : kas_masuk ? "Rp. " + kas_masuk : "";
}
</script>

@endsection
