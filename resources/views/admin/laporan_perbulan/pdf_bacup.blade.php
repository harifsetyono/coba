<!DOCTYPE html>
<html>
<head>
	<title>Laporan Per Bulan</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/bootstrap/css/bootstrap.min.css') }}">
</head>
<body style="width:100%;">

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 align="center">Laporan Kas</h3>
			<h5 align="center">Per Bulan : {{$bulan_tampil}} / {{$tahun_tampil}}</h5>
		</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th >No #</th>
						<th >Tanggal</th>
						<th >Keterangan</th>
						<th >Kat. Jenis</th>
						<th >Masuk (Rp.)</th>
						<th >Keluar (Rp.)</th>
						<th>Saldo (Rp.)</th>
					</tr>
				</thead>
				<tbody>
					<tr>

						<?php


						$bulan_text = "";

						if ($bulan_sebelumnya == "1") {
							$bulan_text = "Januari";
						}elseif ($bulan_sebelumnya == "2") {
							$bulan_text = "Febuari";
						}elseif ($bulan_sebelumnya == "3") {
							$bulan_text = "Maret";
						}elseif ($bulan_sebelumnya == "4") {
							$bulan_text = "April";
						}elseif ($bulan_sebelumnya == "5") {
							$bulan_text = "Mei";
						}elseif ($bulan_sebelumnya == "6") {
							$bulan_text = "Juni";
						}elseif ($bulan_sebelumnya == "7") {
							$bulan_text = "Juli";
						}elseif ($bulan_sebelumnya == "8") {
							$bulan_text = "Agustus";
						}elseif ($bulan_sebelumnya == "9") {
							$bulan_text = "September";
						}elseif ($bulan_sebelumnya == "10") {
							$bulan_text = "Oktober";
						}elseif ($bulan_sebelumnya == "11") {
							$bulan_text = "November";
						}elseif ($bulan_sebelumnya == "12") {
							$bulan_text = "Desember";
						}

						?>

						<td> 1</td>
						<td>01/{{$bulan_tampil}}/{{$tahun_tampil}}</td>
						<td>Kas bulan {{$bulan_text}} {{$tahun_sebelumnya}}</td>
						<td>Sisa Kas</td>
						<td style="text-align:right;">-</td>
						<td style="text-align:right;">-</td>
						<td style="text-align:right;">{{number_format($saldo_bulan_sebelumnya,0,',','.')}}</td>
					</tr>
					<?php $no = 2; ?>
					@foreach($laporan as $data)

					<?php

					$masuk = "";
					$keluar = "";

					if ($data->kas_masuk==0) {
						$masuk = "-";
					}else {
						$masuk = number_format($data->kas_masuk,0,',','.');
					}

					if ($data->kas_keluar==0) {
						$keluar = "-";
					}else {
						$keluar = number_format($data->kas_keluar,0,',','.');
					}

					?>

					<tr>
						<td>{{$no}}</td>
						<td>{{date('d/m/Y', strtotime($data->kas_tanggal))}}</td>
						<td>{{$data->kas_keterangan}}</td>
						<td>{{$data->kategori_nama}}</td>
						<td style="text-align:right;">{{$masuk}}</td>
						<td style="text-align:right;">{{$keluar}}</td>
						<td style="text-align:right;">{{number_format($data->kas_saldo,0,',','.')}}</td>
					</tr>

					<?php $no++; ?>
					@endforeach


				</tbody>
				<tfoot>
					<th style="text-align:center;" colspan="4">TOTAL</th>
					<th style="text-align:right;">{{number_format($total_pemasukan,0,',','.')}} </th>
					<th style="text-align:right;">{{number_format($total_pengeluaran,0,',','.')}} </th>
					<th style="text-align:right;">{{number_format($saldo_bulan_ini,0,',','.')}} </th>
				</tfoot>
			</table>
		</div>

	</div>

</body>
</html>
