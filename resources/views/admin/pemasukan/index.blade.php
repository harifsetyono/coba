<?php $hal = "pemasukan"; ?>
@extends('layouts.admin.master')
@section('title', 'Data Pemasukan')

@section('css')
<link rel="stylesheet" href="{{asset('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pemasukan
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- Horizontal Form -->
      <div class="box box-success">





        <div class="box-body">
          <div class="row">


            <div class="col-xs-4">
              <a href="{{url('pemasukan_create')}}" style=" width:100px;" class="card-body-title"><button class="btn btn-primary">Tambah</button></a>
            </div>
          </div>
        </div>




        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-hover table-striped">
            <thead>
              <tr>
                <th style="width:7%;">No #</th>
                <th style="width:12%;">Tanggal</th>
                <th style="width:30%;">Keterangan</th>
                <th style="width:20%;">Kat. Jenis</th>
                <th style="width:15%;">Nominal (Rp.)</th>
                <th style="width:15%;">Action</th>
              </tr>
            </thead>
            <tbody>



              <?php $no = 1; ?>
              @foreach($pemasukan as $data)

              <?php

              $masuk = "";
              $keluar = "";

              if ($data->kas_masuk==0) {
                $masuk = "-";
              }else {
                $masuk = number_format($data->kas_masuk,0,',','.');
              }


              ?>

              <tr>
                <td>{{$no}}</td>
                <td>{{date('d/m/Y', strtotime($data->kas_tanggal))}}</td>
                <td>{{$data->kas_keterangan}}</td>
                <td>{{$data->kategori_nama}}</td>
                <td style="text-align:right;">{{$masuk}}</td>
                <td>

                  <a href="{{url('pemasukan_edit')}}/{{$data->kas_id}}" style="margin-right:10px;" class="btn btn-success" data-toggle="tooltip" data-placement="botttom" title="Edit Data"><i class="icon ion-edit"></i></a>
                  <a href="{{url('pemasukan_delete')}}/{{$data->kas_id}}" style="margin-right:10px;" onclick="return confirm('Are you sure?');" class="btn btn-danger" data-toggle="tooltip" data-placement="botttom" title="Hapus Data"><i class="icon ion-trash-b"></i></a>

                </td>
              </tr>

              <?php $no++; ?>
              @endforeach


            </tbody>

          </table>
        </div>
        <!-- /.box-body -->

      </div>
      <!-- /.box -->


    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@endsection


@section('js')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script> -->
<script src="{{asset('public/admin/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
// $(function () {
//   //Initialize Select2 Elements
//   $('.select2').select2()
// })
$(document).ready(function() {
  $('.js-example-basic-single').select2({
    // dropdownParent: $(".modal")
  });
});
</script>

<script>
function hanyaAngka(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))

  return false;
  return true;
}
</script>

<script>
$(function () {
  $('#example1').DataTable()
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  })
})
</script>

@endsection
