<?php $hal = "pengeluaran"; ?>
@extends('layouts.admin.master')
@section('title', 'Tambah Pengeluaran')

@section('css')
<link rel="stylesheet" href="{{asset('public/admin/bower_components/select2/dist/css/select2.min.css')}}">

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pengeluaran
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-8">
      <!-- Horizontal Form -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">Edit data pengeluaran</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="{{route('pengeluaran.update')}}" method="POST">
          {{ csrf_field() }}

          @if ($errors->any())
          <div class="alert alert-danger"><ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul></div>
          @endif

          @foreach($pengeluaran as $data)

          <input type="hidden" name="kas_id" value="{{$data->kas_id}}">

          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>

              <div class="input-group col-sm-9">
                <!-- <span class="input-group-addon"><i class="fa  fa-smile-o"></i></span> -->
                <input type="text" readonly class="form-control" required name="" placeholder="Tanggal" value="{{date('d/m/Y', strtotime($data->kas_tanggal))}}">
                <input type="hidden" readonly class="form-control" required name="kas_tanggal" placeholder="Tanggal" value="{{$data->kas_tanggal}}">
                <!-- <span  class="pull-right"><b>*</b> Periksa kembali pemilihan tanggal</span> -->
              </div>
            </div>

            <div class="form-group">
              <label for="users_email" class="col-sm-2 control-label">Keterangan</label>

              <div class="input-group col-sm-9">
                <!-- <span class="input-group-addon">@</span> -->
                <input type="text" class="form-control" value="{{$data->kas_keterangan}}" required name="kas_keterangan" placeholder="Isikan Keterangan">
              </div>
            </div>
            <div class="form-group">
              <label for="kas_masuk" class="col-sm-2 control-label">Kas Keluar (Rp.)</label>

              <div class="input-group col-sm-9">
                <!-- <span class="input-group-addon"><i class="fa  fa-user"></i></span> -->
                <input type="text" onkeypress="return hanyaAngka(event)" value="{{$data->kas_keluar}}" class="form-control text-right" required name="kas_keluar" id="kas_keluar" placeholder="Example: 150000" >
                <b><span id="rupiah" class="rupiah pull-right">Rp. {{number_format($data->kas_keluar,0,',','.')}}</span></b>
                <span class="help-block with-errors pull-right"></span>
              </div>
            </div>
            <!-- /.form-group -->
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Kategori Jenis</label>

              <div class="input-group col-sm-9">
                <!-- <span class="input-group-addon"><i class="fa fa-lock"></i></span> -->
                <select  id="kategori_id" required name="kategori_id" class="form-control js-example-basic-single" style="width: 100%;">
                  <option value="" >--Pilih--</option>
                  @foreach($kategori as $list)
                  <option  value="{{ $list->kategori_id }}" {{ $data->kategori_id==$list->kategori_id ? 'selected' : '' }} > {{ $list->kategori_nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="{{route('pemasukan.index')}}" class="btn btn-warning"  class="card-body-title">Kembali</a>
            <button type="submit" class="btn btn-danger pull-right">SIMPAN</button>
          </div>

          @endforeach

          <!-- /.box-footer -->
        </form>
      </div>
      <!-- /.box -->


    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@endsection


@section('js')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script> -->
<script src="{{asset('public/admin/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{ asset('public/js/sweetalert.min.js') }}"></script>
@include('sweet::alert')

<script>
// $(function () {
//   //Initialize Select2 Elements
//   $('.select2').select2()
// })
$(document).ready(function() {
  $('.js-example-basic-single').select2({
    // dropdownParent: $(".modal")
  });
});
</script>

<script>
function hanyaAngka(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))

  return false;
  return true;
}
</script>

<script type="text/javascript">
var kas_keluar = document.getElementById("kas_keluar");
// var rupiah = document.getElementById("rupiah");
var rupiah = document.getElementById("rupiah");
kas_keluar.addEventListener("keyup", function(e) {
  // tambahkan 'Rp.' pada saat form di ketik
  // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
  // rupiah.value = formatRupiah(this.value, "Rp. ");
  rupiah.innerHTML = formatRupiah(this.value, "Rp. ");
  $('#rupiah').val('coba');
  console.log(rupiah);
});

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix) {
  var number_string = angka.replace(/[^,\d]/g, "").toString(),
  split = number_string.split(","),
  sisa = split[0].length % 3,
  kas_keluar = split[0].substr(0, sisa),
  ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if (ribuan) {
    separator = sisa ? "." : "";
    kas_keluar += separator + ribuan.join(".");
  }

  kas_keluar = split[1] != undefined ? kas_keluar + "," + split[1] : kas_keluar;
  return prefix == undefined ? kas_keluar : kas_keluar ? "Rp. " + kas_keluar : "";
}
</script>

@endsection
