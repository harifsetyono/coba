/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 10.1.16-MariaDB : Database - gudang_dlopo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`gudang_dlopo` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `gudang_dlopo`;

/*Table structure for table `kas` */

DROP TABLE IF EXISTS `kas`;

CREATE TABLE `kas` (
  `kas_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_id` int(11) DEFAULT NULL,
  `kas_tanggal` date DEFAULT NULL,
  `kas_keterangan` text,
  `kas_masuk` int(11) DEFAULT NULL,
  `kas_keluar` int(11) DEFAULT NULL,
  `kas_saldo` int(11) DEFAULT NULL,
  `kas_jenis` enum('1','2') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`kas_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `kas` */

insert  into `kas`(`kas_id`,`kategori_id`,`kas_tanggal`,`kas_keterangan`,`kas_masuk`,`kas_keluar`,`kas_saldo`,`kas_jenis`,`created_at`,`updated_at`) values 
(1,4,'2019-10-15','Jual Rosok',200000,0,200000,'1','2019-10-15 08:54:45','2019-10-15 08:54:45'),
(2,4,'2019-10-15','Jual Botol',300000,0,500000,'1','2019-10-15 08:55:37','2019-10-15 08:55:37'),
(3,2,'2019-10-15','Beli aqua',0,100000,400000,'2','2019-10-15 08:55:59','2019-10-15 08:55:59'),
(4,5,'2019-10-15','Beli bensin',0,150000,250000,'2','2019-10-15 08:56:19','2019-10-15 08:56:19'),
(5,4,'2019-10-15','Jual besi bekas',400000,0,700000,'1','2019-10-15 09:10:24','2019-10-15 09:01:50'),
(7,4,'2019-10-15','Jual kaleng bekas',120000,0,820000,'1','2019-10-15 12:53:54','2019-10-15 12:53:54'),
(9,5,'2019-10-15','Beli pertamax',0,55000,770000,'2','2019-10-15 13:24:26','2019-10-15 13:24:26');

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_nama` varchar(200) DEFAULT NULL,
  `kategori_jenis` enum('1','2') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `kategori` */

insert  into `kategori`(`kategori_id`,`kategori_nama`,`kategori_jenis`,`created_at`,`updated_at`) values 
(1,'Sisa Kas','1','2019-10-06 09:30:29','2019-10-06 09:30:29'),
(2,'Beban air minum','2','2019-10-06 09:27:02','2019-10-06 09:27:02'),
(4,'Penjualan','1','2019-10-06 11:14:51','2019-10-06 11:14:51'),
(5,'Beban bensin','2','2019-10-06 11:55:30','2019-10-06 11:55:30');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `tbl_group` */

DROP TABLE IF EXISTS `tbl_group`;

CREATE TABLE `tbl_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_nama` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_group` */

insert  into `tbl_group`(`group_id`,`group_nama`,`created_at`,`updated_at`) values 
(1,'Administrator','2019-08-08 11:26:39','0000-00-00 00:00:00'),
(3,'Pengguna','2019-08-08 12:26:15','2019-08-08 12:26:15'),
(4,'Operator','2019-08-12 13:31:24','2019-08-12 13:31:24');

/*Table structure for table `tbl_menu` */

DROP TABLE IF EXISTS `tbl_menu`;

CREATE TABLE `tbl_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_nama` varchar(200) NOT NULL,
  `menu_link` varchar(200) DEFAULT NULL,
  `menu_id_parent` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_menu` */

insert  into `tbl_menu`(`menu_id`,`menu_nama`,`menu_link`,`menu_id_parent`,`created_at`,`updated_at`) values 
(11,'Manajemen User','manajemen_user',0,'2019-08-13 09:08:33','2019-08-13 09:08:33'),
(12,'Group','group',11,'2019-08-13 09:08:56','2019-08-13 09:08:56'),
(13,'Users','master_user',11,'2019-08-13 09:09:19','2019-08-13 09:09:19'),
(14,'Menu','menu',11,'2019-08-13 09:09:36','2019-08-13 09:09:36'),
(23,'Kas Gudang','kas_gudang',0,'2019-10-06 09:08:12','2019-10-06 09:08:12'),
(24,'Kategori Kas','kategori',23,'2019-10-06 09:08:30','2019-10-06 09:08:30'),
(25,'Pemasukan','pemasukan',23,'2019-10-06 09:38:46','2019-10-06 09:38:46'),
(26,'Pengeluaran','pengeluaran',23,'2019-10-06 09:38:58','2019-10-06 09:38:58'),
(27,'Laporan Kas','laporan_perbulan',23,'2019-10-06 12:34:35','2019-10-06 12:34:35');

/*Table structure for table `tbl_t_user` */

DROP TABLE IF EXISTS `tbl_t_user`;

CREATE TABLE `tbl_t_user` (
  `t_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`t_user_id`),
  KEY `t_user__group_relation` (`group_id`),
  KEY `t_user__menu_relation` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=342 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_t_user` */

insert  into `tbl_t_user`(`t_user_id`,`group_id`,`menu_id`,`created_at`,`updated_at`) values 
(333,1,11,'2019-10-06 12:34:46','0000-00-00 00:00:00'),
(334,1,12,'2019-10-06 12:34:46','0000-00-00 00:00:00'),
(335,1,13,'2019-10-06 12:34:46','0000-00-00 00:00:00'),
(336,1,14,'2019-10-06 12:34:46','0000-00-00 00:00:00'),
(337,1,23,'2019-10-06 12:34:46','0000-00-00 00:00:00'),
(338,1,24,'2019-10-06 12:34:46','0000-00-00 00:00:00'),
(339,1,25,'2019-10-06 12:34:46','0000-00-00 00:00:00'),
(340,1,26,'2019-10-06 12:34:46','0000-00-00 00:00:00'),
(341,1,27,'2019-10-06 12:34:46','0000-00-00 00:00:00');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `users_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `group_relation` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`group_id`,`name`,`email`,`password`,`users_email`,`remember_token`,`created_at`,`updated_at`) values 
(2,1,'harif','harif','$2y$10$nEM1BPtssfVaHXa1OALtguW3osZjm9/4vQIeJQ5ef/fBW3bcDE/S2','harif@gmail.com','Pn9XaRC5hJ0sYrrLzG5UxtdM0Hchouv5pm8Gxrbq0aENRPhiBt20Vf9iSZHA',NULL,'2019-10-05 20:05:24'),
(3,3,'setyo','setyo','$2y$10$rz3K/oAH/4ZQYDezGv0RP.WQeTSqAjkNLkNIL7CZptujpPEz0EhIm','setyon@gmail.com','mBvRgHfwHWgqmzDgkRfheibGSGspTPtgW2iaYDmom6u3Mi78xAVikLjvMF8s','2019-08-08 14:16:08','2019-08-08 14:42:05');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
