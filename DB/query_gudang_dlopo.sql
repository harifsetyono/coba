CREATE DATABASE gudang_dlopo

SELECT kas.kas_id, kas.kas_keterangan, kat.kategori_nama, kas.kas_masuk, kas.kas_keluar, kas.kas_saldo 
FROM kas, kategori kat
WHERE kas.kategori_id = kat.kategori_id
AND MONTH(kas.kas_tanggal) = '10' AND YEAR(kas.kas_tanggal) = '2019'
ORDER BY kas.kas_id

SELECT kas.kas_id, kas.kas_keterangan, kat.kategori_nama, kas.kas_masuk, kas.kas_keluar, IF(kas.kas_keluar=0,'mantab','ora') AS hasil
FROM kas, kategori kat
WHERE kas.kategori_id = kat.kategori_id
AND MONTH(kas.kas_tanggal) = '10' AND YEAR(kas.kas_tanggal) = '2019'
ORDER BY kas.kas_id

SELECT kas.kas_id, kas.kas_keterangan, kat.kategori_nama, kas.kas_masuk, kas.kas_keluar, 
(CASE 
	WHEN kas.kas_keluar=0 THEN "Mantab" 
	WHEN kas.kas_masuk=0 THEN "ora Mantab" 
END) AS hasil
FROM kas, kategori kat
WHERE kas.kategori_id = kat.kategori_id
AND MONTH(kas.kas_tanggal) = '10' AND YEAR(kas.kas_tanggal) = '2019'
ORDER BY kas.kas_id

CREATE TABLE `gudang_dlopo`.`trx` (`nominal` FLOAT NOT NULL, `kode` CHAR(10) NOT NULL) ENGINE = INNODB;

INSERT INTO `gudang_dlopo`.`trx` (`nominal`, `kode`) VALUES ('3434', 'k'), ('3434', 'k'), ('4434', 'k'), ('544', 'd'), ('453', 'd');

SELECT @k:=IF(kode="k",nominal,0) AS Kredit,@d:=IF(kode="d",nominal,0) AS
 debet, @s:=@s+@k-@d AS saldo FROM trx
 
 
SELECT kas.kas_id, kas.kas_keterangan, kat.kategori_nama, kas.kas_masuk, kas.kas_keluar, 
@saldo_awal:=@saldo_awal+kas.kas_masuk-kas.kas_keluar AS saldocoy
FROM kas, kategori kat
WHERE kas.kategori_id = kat.kategori_id
AND MONTH(kas.kas_tanggal) = '10' AND YEAR(kas.kas_tanggal) = '2019'
ORDER BY kas.kas_id

SET @saldo = 0;

SELECT  kas_keterangan, kas_masuk, kas_keluar, kas_jenis, IF(@saldo = 0, IF(kas_keterangan = 'abc',@saldo := IF(kas_jenis = '1', kas_masuk,kas_keluar),0), 
@saldo := @saldo + IF(kas_jenis = '1', kas_masuk,kas_keluar) - 
IF(kas_jenis = '2', kas_masuk,kas_keluar)) saldo FROM (SELECT * FROM kas) a

//=======================================

CREATE TABLE bank AS
    SELECT 'MD001' id, 1000 debet, 0 kredit UNION ALL
    SELECT 'MD001' id, 2000 debet, 0 kredit UNION ALL
    SELECT 'MD001' id, 0 debet, 500 kredit UNION ALL
    SELECT 'MD002' id, 1500 debet, 0 kredit UNION ALL
    SELECT 'MD002' id, 0 debet, 500 kredit UNION ALL
    SELECT 'MD002' id, 1000 debet, 0 kredit
    
    

SELECT t1.id, t1.debet, t1.kredit
     , CASE WHEN t1.id<>t2.id THEN @saldo:=debet-kredit
            ELSE @saldo:=@saldo+debet-kredit END saldo
     FROM (
       SELECT id, debet, kredit, @baris1:=@baris1+1 baris
       FROM bank, (SELECT @baris1 := 0) tx
       ORDER BY id
     ) t1
     LEFT JOIN (
       SELECT id, @baris2:=@baris2+1 baris
       FROM bank, (SELECT @baris2 := 1) tx
       ORDER BY id
     ) t2 ON t1.baris=t2.baris
     JOIN (
       SELECT @saldo:=0
     ) t3
     ;
     
     
     SELECT t1.id, t1.debet, t1.kredit, @saldo:=@saldo+t1.debet-t1.kredit AS saldo FROM bank t1 JOIN (
       SELECT @saldo:=0
     ) t3
     ;
     //========================
SELECT kas.kas_id, kas.kas_keterangan, kat.kategori_nama, kas.kas_masuk, kas.kas_keluar, @saldo:=@saldo+kas_masuk-kas_keluar AS saldo 
FROM kas
JOIN kategori kat ON kas.kategori_id = kat.kategori_id
JOIN (
	SELECT @saldo:=0
) t3
WHERE 
MONTH(kas.kas_tanggal) = '10' AND YEAR(kas.kas_tanggal) = '2019'
ORDER BY kas.kas_id;
     //===========================
     
SELECT t1.id, t1.debet, t1.kredit
     , CASE WHEN t1.id<>t2.id THEN @saldo:=debet-kredit
            ELSE @saldo:=@saldo+debet-kredit END saldo
     FROM (
       SELECT id, debet, kredit, @baris1:=@baris1+1 baris
       FROM bank, (SELECT @baris1 := 0) tx
       ORDER BY id
     ) t1
     LEFT JOIN (
       SELECT id, @baris2:=@baris2+1 baris
       FROM bank, (SELECT @baris2 := 1) tx
       ORDER BY id
     ) t2 ON t1.baris=t2.baris
     JOIN (
       SELECT @saldo:=0
     ) t3
     ;


//========================= Hitung Saldo ================================

SELECT kas.kas_id, kas.kas_tanggal,kas.kas_keterangan, kat.kategori_nama, kas.kas_masuk, kas.kas_keluar, @saldo:=@saldo+kas_masuk-kas_keluar AS kas_saldo 
FROM kas
JOIN kategori kat ON kas.kategori_id = kat.kategori_id
JOIN (
	SELECT @saldo:=0
) saldo
WHERE 
MONTH(kas.kas_tanggal) = '10' AND YEAR(kas.kas_tanggal) = '2019'
ORDER BY kas.created_at ASC;

SELECT kas.kas_id, kas.kas_keterangan, kat.kategori_nama, kas.kas_masuk, kas.kas_keluar, @saldo:=@saldo+kas_masuk-kas_keluar AS saldo 
FROM kas
JOIN kategori kat ON kas.kategori_id = kat.kategori_id
JOIN (
	SELECT @saldo:=0
) t3
WHERE 
MONTH(kas.kas_tanggal) = '10' AND YEAR(kas.kas_tanggal) = '2019'
ORDER BY kas.kas_id DESC
LIMIT 1 ;

