<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {

  Route::get('index_admin.html', 'PecahTemplateAdminController@index');
  Route::resource('admin_user', 'AdminUserController');


  //MANAJEMEN USER ===================================================================================================================================

  //  ================================================ GROUP ==========================================================
  Route::resource('group', 'GroupController');
  Route::get('data_group', 'GroupController@listData')->name('data_group');
  //  ================================================ END GROUP ==========================================================

  //  ================================================ MASTER USER ==========================================================
  Route::resource('master_user', 'MasterUserController');
  Route::get('data_master_user', 'MasterUserController@listData')->name('data_master_user');
  //  ================================================ END MASTER USER ==========================================================

  //  ================================================ MENU ==========================================================
  Route::resource('menu', 'MenuController');
  Route::get('data_menu', 'MenuController@listData')->name('data_menu');
  //  ================================================ END MENU ==========================================================

  //  ================================================ T USER ==========================================================
  Route::resource('t_user', 'TUserController');
  //  ================================================ END T USER ==========================================================

  //  ================================================ PEMBELIAN ==========================================================
  Route::get('data_pembelian', 'PembelianController@listData')->name('data_pembelian');
  Route::post('update_total_pembelian', 'PembelianController@totalBarang')->name('total_pembelian');
  Route::resource('pembelian', 'PembelianController');
  //  ================================================ END PEMBELIAN ==========================================================

  //  ================================================ DETAIL PEMBELIAN ==========================================================
  Route::get('get_data_barang/{id}', 'DetailPembelianController@getDataBarang')->name('get_pembelian');
  Route::get('detail_data_pembelian', 'DetailPembelianController@listData')->name('detail_data_pembelian');
  Route::resource('detail_pembelian', 'DetailPembelianController');
  //  ================================================ END DETAIL PEMBELIAN ==========================================================

  //  ================================================ DETAIL PEMBELIAN2 ==========================================================
  Route::resource('detail_pembelian2', 'DetailPembelian2Controller');
  //  ================================================ END DETAIL PEMBELIAN ==========================================================

  //END MANAJEMEN USER ===================================================================================================================================




  //KAS ===================================================================================================================================

  //  ================================================ KATEGORI KAS ==========================================================
  Route::resource('kategori', 'KategoriController');
  Route::get('data_kategori', 'KategoriController@listData')->name('data_kategori');
  //  ================================================ END KATEGORI KAS ==========================================================

  //  ================================================ PEMASUKAN ==========================================================
  Route::get('pemasukan', 'PemasukanController@index')->name('pemasukan.index');
  Route::get('pemasukan_create', 'PemasukanController@create');
  Route::post('pemasukan_store', 'PemasukanController@store')->name('pemasukan.store');
  Route::get('pemasukan_edit/{id}', 'PemasukanController@edit');
  Route::post('pemasukan_update', 'PemasukanController@update')->name('pemasukan.update');
  Route::get('pemasukan_delete/{id}', 'PemasukanController@delete');

  //  ================================================ END PEMASUKAN ==========================================================

  //  ================================================ PENGELUARAN ==========================================================
  Route::get('pengeluaran', 'PengeluaranController@index')->name('pengeluaran.index');
  Route::get('pengeluaran_create', 'PengeluaranController@create');
  Route::post('pengeluaran_store', 'PengeluaranController@store')->name('pengeluaran.store');
  Route::get('pengeluaran_edit/{id}', 'PengeluaranController@edit');
  Route::post('pengeluaran_update', 'PengeluaranController@update')->name('pengeluaran.update');
  Route::get('pengeluaran_delete/{id}', 'PengeluaranController@delete');
  //  ================================================ END PENGELUARAN ==========================================================

  //  ================================================ LAPORAN KAS ==========================================================
  Route::get('laporan_perbulan', 'LaporanPerbulanController@index')->name('laporan_perbulan.index');
  Route::post('laporan_perbulan_cari', 'LaporanPerbulanController@cari')->name('laporan_perbulan_cari.cari');
  Route::get('laporan_perbulan_pdf/{bulan}/{tahun}', 'LaporanPerbulanController@laporanPerBulanPdf');
  //  ================================================ END LAPORAN KAS ==========================================================

  //END KAS ===================================================================================================================================


// adhajdahj


  //jhj


});
